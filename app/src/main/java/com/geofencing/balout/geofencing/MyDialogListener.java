package com.geofencing.balout.geofencing;

import com.google.android.gms.location.Geofence;

public interface MyDialogListener {
    void userSubmitedAValue(String lattitude,String longitude,String type);
}
