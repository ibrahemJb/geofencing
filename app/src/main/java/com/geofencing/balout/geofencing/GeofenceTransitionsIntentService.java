package com.geofencing.balout.geofencing;

import android.app.IntentService;
import android.app.Notification;
import android.app.NotificationManager;
import android.content.Intent;
import android.graphics.Color;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.NotificationManagerCompat;
import android.util.Log;

import com.google.android.gms.location.Geofence;
import com.google.android.gms.location.GeofencingEvent;

import java.util.ArrayList;
import java.util.List;


public class GeofenceTransitionsIntentService extends IntentService {

    public GeofenceTransitionsIntentService() {
        super("GeofenceTransitionsIntentService");
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        GeofencingEvent geofencingEvent = GeofencingEvent.fromIntent(intent);

        if (geofencingEvent.hasError()) {
            return;
        }
        int transaction = geofencingEvent.getGeofenceTransition();
        List<Geofence> triggerdGeofences = MyProperties.getInstance().triggerdGeofences;
        for(Geofence trigger : geofencingEvent.getTriggeringGeofences()) {
            int index = -1;
            int length = triggerdGeofences.size();
            for (int i=0;i<length;i++) {
                if(triggerdGeofences.get(i).getRequestId().equals(trigger.getRequestId())){
                    index = i;
                }
            }
            Log.d("geofence", trigger.toString());
            if (transaction == Geofence.GEOFENCE_TRANSITION_ENTER) {
                if(index==-1){
                    MyProperties.getInstance().triggerdGeofences.add(trigger);
                    String type[] = trigger.getRequestId().split("-");
                    addNotfiy("Approaching a " + type[0],Integer.parseInt(type[1]));
                }
            }
            else if(transaction == Geofence.GEOFENCE_TRANSITION_EXIT){
                if(index != -1){
                    MyProperties.getInstance().triggerdGeofences.remove(index);
                }
            }
        }
    }
    private void addNotfiy(String msg,int notId) {
        NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(this,Constants.CHANNELID)
                .setSmallIcon(R.drawable.common_google_signin_btn_icon_dark)
                .setContentTitle("Careful !")
                .setColorized(true)
                .setColor(Color.RED)
                .setContentText(msg)
                .setDefaults(Notification.DEFAULT_ALL)
                .setPriority(NotificationManager.IMPORTANCE_HIGH);
        NotificationManagerCompat notificationManager = NotificationManagerCompat.from(this);
        notificationManager.notify(notId, mBuilder.build());
        geofencingForegroundService.speaK(msg);
        Intent x = new Intent(Constants.ACTION_UPDATE_UI);
        x.putExtra("msg",msg);
        sendBroadcast(x);
    }
}
