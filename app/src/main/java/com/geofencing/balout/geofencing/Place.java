package com.geofencing.balout.geofencing;

import android.location.Location;

import com.google.android.gms.location.Geofence;

public class Place {
    Geofence geofence;
    Location location;
    String type;
    public Place(Geofence geofence, Location location,String type) {
        this.geofence = geofence;
        this.location = location;
        this.type = type;
    }
}