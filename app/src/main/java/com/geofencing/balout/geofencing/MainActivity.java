package com.geofencing.balout.geofencing;

import android.Manifest;
import android.app.Activity;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.NotificationCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.app.AppCompatCallback;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity
        implements CompoundButton.OnCheckedChangeListener,
        View.OnClickListener

{
    private DrawerLayout mDrawerLayout;
    private NavigationView navigationView;
    public static Switch sw_stopSigns,sw_trafficLights,sw_humps,sw_pedestrainCrossings,sw_yeildSigns,sw_humpSigns,sw_pedestrainCrossingSigns;
    private EditText et_radius,et_interval,et_fastestInterval;
    private FloatingActionButton fab_clearList;
    private TextView tv_haederNotice;

    private int radius = 100;
    private int interval = 10;
    private int fastestInterval = 5;
    private int old_radius=radius,old_interval=interval,old_fastestInterval= fastestInterval;


    private RecyclerView rv_activity;
    private ArrayList<String> al_activity_rv;
    private RecyclerView.Adapter mAdapter;
    private BroadcastReceiver broadcastReceiver;
    private SharedPreferences sharedPref;

    private AddLocationDialog addLocationDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        if(ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED){
            ActivityCompat.requestPermissions(this,new String[]{Manifest.permission.ACCESS_FINE_LOCATION},Constants.FINE_LOCATION_PERMESIION_REQUEST_CODE);
        }
        else{
            initializeAndStart();
        }
    }

    private void initializeAndStart(){
        createNotificationChannel();
        createForegroundNotificationChannel();
        sharedPref = this.getPreferences(Context.MODE_PRIVATE);
        if(!sharedPref.contains(Constants.INITIALIZED_S)){
            initializeSharedPrefs();
        }
        initialize();
        registerBroadcastReceivers();
        Intent intent = new Intent(MainActivity.this, geofencingForegroundService.class);
        Bundle b = new Bundle();
        b.putInt(Constants.RADUIS_S,radius);
        b.putInt(Constants.INTERVAL_S,interval);
        b.putInt(Constants.FASTEST_INTERVAL_S,fastestInterval);
        intent.putExtra("pram",b);
        intent.setAction(Constants.ACTION_START_FOREGROUND_SERVICE);
        startService(intent);
    }
    @Override
    protected void onDestroy() {
        super.onDestroy();
        unregisterReceiver(broadcastReceiver);
    }

    private void initializeSharedPrefs(){
        SharedPreferences.Editor ed = sharedPref.edit();
        ed.putBoolean(Constants.INITIALIZED_S, true);
        ed.putString(Constants.RADUIS_S,Constants.DEFAULTRADIUS);
        ed.putString(Constants.INTERVAL_S,Constants.DEFAULTINTERVAL);
        ed.putString(Constants.FASTEST_INTERVAL_S,Constants.DEFAULTFASTESTINTERVAL);
        ed.putBoolean(Constants.STOPSIGN_S,true);
        ed.putBoolean(Constants.TRAFFIC_LIGHT_S,true);
        ed.putBoolean(Constants.HUMP_S,true);
        ed.putBoolean(Constants.PEDESTRAIN_CROSSING_S,true);
        ed.putBoolean(Constants.YEILD_SIGN_S,true);
        ed.putBoolean(Constants.HUMP_SIGN_S,true);
        ed.putBoolean(Constants.PEDESTRAIN_CROSSING_SIGN_S,true);
        ed.apply();
    }

    private void initialize(){
        //================Drawer button initialize start==========================
        mDrawerLayout = findViewById(R.id.drawer_layout);
        navigationView = findViewById(R.id.nav_view);
        tv_haederNotice = navigationView.getHeaderView(0).findViewById(R.id.tv_header_notice);
        mDrawerLayout.addDrawerListener(drawerListener);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_dehaze_black_24dp);
        //================Drawer button initialize end==========================

        //=================Drawer Menu initialize start ======================
        Menu menu = navigationView.getMenu();

        //============== switches start ===================
        sw_stopSigns = (Switch) menu.findItem(R.id.sw_stop_signs).getActionView();
        sw_trafficLights =(Switch) menu.findItem(R.id.sw_traffic_lights).getActionView();
        sw_humps = (Switch) menu.findItem(R.id.sw_humps).getActionView();
        sw_pedestrainCrossings = (Switch) menu.findItem(R.id.sw_pedestrain_crossings).getActionView();
        sw_yeildSigns = (Switch) menu.findItem(R.id.sw_yeild_signs).getActionView();
        sw_humpSigns = (Switch) menu.findItem(R.id.sw_hump_signs).getActionView();
        sw_pedestrainCrossingSigns = (Switch) menu.findItem(R.id.sw_pedestrain_crossingSigns).getActionView();

        sw_stopSigns.setChecked(sharedPref.getBoolean(Constants.STOPSIGN_S,true));
        MyProperties.getInstance().StopSigns = sw_stopSigns.isChecked();
        sw_trafficLights.setChecked(sharedPref.getBoolean(Constants.TRAFFIC_LIGHT_S,true));
        MyProperties.getInstance().TrafficLights = sw_trafficLights.isChecked();
        sw_humps.setChecked(sharedPref.getBoolean(Constants.HUMP_S,true));
        MyProperties.getInstance().Humps = sw_humps.isChecked();
        sw_pedestrainCrossings.setChecked(sharedPref.getBoolean(Constants.PEDESTRAIN_CROSSING_S,true));
        MyProperties.getInstance().PedestrainCrossings = sw_pedestrainCrossings.isChecked();
        sw_yeildSigns.setChecked(sharedPref.getBoolean(Constants.YEILD_SIGN_S,true));
        MyProperties.getInstance().YeildSigns = sw_yeildSigns.isChecked();
        sw_humpSigns.setChecked(sharedPref.getBoolean(Constants.HUMP_SIGN_S,true));
        MyProperties.getInstance().HumpSigns = sw_humpSigns.isChecked();
        sw_pedestrainCrossingSigns.setChecked(sharedPref.getBoolean(Constants.PEDESTRAIN_CROSSING_SIGN_S,true));
        MyProperties.getInstance().PedestrainCrossingsSigns = sw_pedestrainCrossingSigns.isChecked();

        sw_stopSigns.setOnCheckedChangeListener(this);
        sw_trafficLights.setOnCheckedChangeListener(this);
        sw_humps.setOnCheckedChangeListener(this);
        sw_pedestrainCrossings.setOnCheckedChangeListener(this);
        sw_yeildSigns.setOnCheckedChangeListener(this);
        sw_humpSigns.setOnCheckedChangeListener(this);
        sw_pedestrainCrossingSigns.setOnCheckedChangeListener(this);
        //=============== swithces end ====================

        //=============== edit texts start==================
        et_radius = (EditText) menu.findItem(R.id.et_radius).getActionView();
        et_interval = (EditText) menu.findItem(R.id.et_interval).getActionView();
        et_fastestInterval = (EditText) menu.findItem(R.id.et_fastest_interval).getActionView();

        radius =Integer.parseInt(sharedPref.getString(Constants.RADUIS_S,Constants.DEFAULTRADIUS));
        old_radius = radius;
        et_radius.setText(Integer.toString(radius));
        MyProperties.getInstance().Radius = radius;

        interval = Integer.parseInt(sharedPref.getString(Constants.INTERVAL_S,Constants.DEFAULTINTERVAL));
        old_interval = interval;
        et_interval.setText(Integer.toString(interval));
        MyProperties.getInstance().Interval = interval;

        fastestInterval = Integer.parseInt(sharedPref.getString(Constants.FASTEST_INTERVAL_S,Constants.DEFAULTFASTESTINTERVAL));
        old_fastestInterval=fastestInterval;
        et_fastestInterval.setText(Integer.toString(fastestInterval));
        MyProperties.getInstance().Fastest_Interval=fastestInterval;

        et_radius.addTextChangedListener(et_watcher);
        et_interval.addTextChangedListener(et_watcher);
        et_fastestInterval.addTextChangedListener(et_watcher);
        //=============== edit texts end==================

        //=================Drawer Menu initialize end ======================

        //=================FAB initialize start ======================
        fab_clearList = findViewById(R.id.fab_clear_list);
        fab_clearList.setOnClickListener(this);
        //=================FAB initialize end ======================

        //=================Recycler View initialize start ===============
        rv_activity = findViewById(R.id.rv_activity);
        initializeRecyclerView();
        //=================Recycler View initialize end ===============

    }

    private void initializeRecyclerView(){
        // use this setting to improve performance if you know that changes
        // in content do not change the layout size of the RecyclerView

        // use a linear layout manager
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(this);
        rv_activity.setLayoutManager(mLayoutManager);

        // specify an adapter (see also next example)
        al_activity_rv = new ArrayList<>(20);
        al_activity_rv.add("Using radius: " + radius + "m");
        mAdapter = new notificationAdapter(al_activity_rv);
        rv_activity.setAdapter(mAdapter);
    }

    private void registerBroadcastReceivers() {
        broadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                if(intent.getAction().equals("finishActivity")){
                    finish();
                }
                else{
                    al_activity_rv.add(0, intent.getStringExtra("msg"));
                    mAdapter.notifyItemInserted(0);
                }
            }
        };
        IntentFilter progressfilter = new IntentFilter(Constants.ACTION_UPDATE_UI);
        progressfilter.addAction("finishActivity");
        registerReceiver(broadcastReceiver, progressfilter);
    }

    public void removeAllCards() {
        al_activity_rv.clear();
        mAdapter.notifyDataSetChanged();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.action_bar_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home: {
                //if drawer is opened close it, if its closed open it.
                if (mDrawerLayout.isDrawerOpen(GravityCompat.START)) {
                    mDrawerLayout.closeDrawer(GravityCompat.START);
                } else {
                    mDrawerLayout.openDrawer(GravityCompat.START);
                }
                return true;
            }
            case R.id.add_location:{
                addLocationDialog = new AddLocationDialog (MainActivity.this);
                addLocationDialog.setMyDialogListener(new MyDialogListener() {
                    @Override
                    public void userSubmitedAValue(String lattitude, String longitude, String type) {
                        Database db = new Database(MainActivity.this);
                        db.insertIntoDB(lattitude,longitude,type);
                        addLocationDialog.dismiss();
                        sendBroadcast(new Intent(Constants.ACTION_UPDATE_GEOFENCELIST));
                    }
                });
                addLocationDialog.show();
            }
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        StringBuilder msg = new StringBuilder("");
        if (isChecked) {
            msg.append("You should be getting %s notificaitons normally.");
        } else {
            msg.append("You wont be getting %s notifications anymore.");
        }
        SharedPreferences.Editor ed = sharedPref.edit();
        switch(buttonView.getId()){
            case R.id.sw_stop_signs:{
                ed.putBoolean(Constants.STOPSIGN_S,isChecked);
                MyProperties.getInstance().StopSigns = isChecked;
                msg.replace(0, msg.length(), String.format(msg.toString(), "\"stop sign\""));
                break;
            }
            case R.id.sw_traffic_lights:{
                ed.putBoolean(Constants.TRAFFIC_LIGHT_S,isChecked);
                MyProperties.getInstance().TrafficLights = isChecked;
                msg.replace(0, msg.length(), String.format(msg.toString(), "\"traffic light\""));
                break;
            }
            case R.id.sw_humps:{
                ed.putBoolean(Constants.HUMP_S,isChecked);
                MyProperties.getInstance().Humps= isChecked;
                msg.replace(0, msg.length(), String.format(msg.toString(), "\"Hump\""));
                break;
            }
            case R.id.sw_pedestrain_crossings:{
                ed.putBoolean(Constants.PEDESTRAIN_CROSSING_S,isChecked);
                MyProperties.getInstance().PedestrainCrossings = isChecked;
                msg.replace(0, msg.length(), String.format(msg.toString(), "\"Pedestrain crossing\""));
                break;
            }
            case R.id.sw_yeild_signs:{
                ed.putBoolean(Constants.YEILD_SIGN_S,isChecked);
                MyProperties.getInstance().YeildSigns = isChecked;
                msg.replace(0, msg.length(), String.format(msg.toString(), "\"yield sign\""));
                break;
            }
            case R.id.sw_hump_signs:{
                ed.putBoolean(Constants.HUMP_SIGN_S,isChecked);
                MyProperties.getInstance().HumpSigns=isChecked;
                msg.replace(0, msg.length(), String.format(msg.toString(), "\"Hump sign\""));
                break;
            }
            case R.id.sw_pedestrain_crossingSigns:{
                ed.putBoolean(Constants.PEDESTRAIN_CROSSING_SIGN_S,isChecked);
                MyProperties.getInstance().PedestrainCrossingsSigns=isChecked;
                msg.replace(0, msg.length(), String.format(msg.toString(), "\"Pedestrain crossing sign\""));
                break;
            }
        }
        ed.apply();
        al_activity_rv.add(0,msg.toString());
        mAdapter.notifyItemInserted(0);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.fab_clear_list:{
                removeAllCards();
            }
        }
    }

    private void createNotificationChannel() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            int importance = NotificationManager.IMPORTANCE_HIGH;
            NotificationChannel channel = new NotificationChannel(Constants.CHANNELID, Constants.CHANNELNAME, importance);
            channel.setDescription(Constants.CHANNELDESCRIPTION);
            NotificationManager notificationManager = getSystemService(NotificationManager.class);
            notificationManager.createNotificationChannel(channel);
        }
    }
    private void createForegroundNotificationChannel() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            int importance = NotificationManager.IMPORTANCE_MIN;
            NotificationChannel channel = new NotificationChannel(Constants.FOREGROUNDCHANNELID,
                    Constants.FOREGROUNDCHANNELNAME, NotificationManager.IMPORTANCE_LOW);
            channel.setDescription(Constants.FOREGROUNDCHANNELDESCRIPTION);
            NotificationManager notificationManager = getSystemService(NotificationManager.class);
            notificationManager.createNotificationChannel(channel);
        }
    }

    TextWatcher et_watcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {

        }

        @Override
        public void afterTextChanged(Editable editable) {
            String text = editable.toString();
            SharedPreferences.Editor ed = sharedPref.edit();
            if(editable == et_radius.getText()){
                if(text.isEmpty()){
                    tv_haederNotice.setText(R.string.header_notice_empty_radius);
                    tv_haederNotice.setVisibility(View.VISIBLE);
                    ed.putString(Constants.RADUIS_S,Constants.DEFAULTRADIUS);
                }
                else if(Integer.parseInt(text) < 30){
                    tv_haederNotice.setText(R.string.header_notice_less_than_thirty);
                    tv_haederNotice.setVisibility(View.VISIBLE);
                    radius = 30;
                    ed.putString(Constants.RADUIS_S,Integer.toString(radius));
                }
                else{
                    tv_haederNotice.setVisibility(View.INVISIBLE);
                    radius = Integer.parseInt(text);
                    ed.putString(Constants.RADUIS_S,text);
                }
            }
            else if(editable == et_interval.getText()){
                if(text.isEmpty()){
                    tv_haederNotice.setText(R.string.header_notice_empty_interval);
                    tv_haederNotice.setVisibility(View.VISIBLE);
                    ed.putString(Constants.INTERVAL_S,Constants.DEFAULTINTERVAL);
                }
                else if(Integer.parseInt(text) < 1){
                    tv_haederNotice.setText(R.string.header_notice_interval_less_than_1);
                    tv_haederNotice.setVisibility(View.VISIBLE);
                    interval = 1;
                    ed.putString(Constants.INTERVAL_S,Integer.toString(interval));
                }
                else{
                    tv_haederNotice.setVisibility(View.INVISIBLE);
                    interval = Integer.parseInt(text);
                    ed.putString(Constants.INTERVAL_S,text);
                }
            }
            else if(editable == et_fastestInterval.getText()){
                if(text.isEmpty()){
                    tv_haederNotice.setText(R.string.header_notice_fastest_interval_empty_radius);
                    tv_haederNotice.setVisibility(View.VISIBLE);
                    ed.putString(Constants.FASTEST_INTERVAL_S,Constants.DEFAULTFASTESTINTERVAL);
                }
                else{
                    tv_haederNotice.setVisibility(View.INVISIBLE);
                    fastestInterval = Integer.parseInt(text);
                    ed.putString(Constants.FASTEST_INTERVAL_S,text);
                }
            }
            ed.apply();
        }
    };

    DrawerLayout.DrawerListener drawerListener = new DrawerLayout.DrawerListener() {
        @Override
        public void onDrawerSlide(@NonNull View view, float v) {

        }

        @Override
        public void onDrawerOpened(@NonNull View view) {

        }

        @Override
        public void onDrawerClosed(@NonNull View view) {
            et_radius.setText(sharedPref.getString(Constants.RADUIS_S,Constants.DEFAULTRADIUS));
            et_interval.setText(sharedPref.getString(Constants.INTERVAL_S,Constants.DEFAULTINTERVAL));
            et_fastestInterval.setText(sharedPref.getString(Constants.FASTEST_INTERVAL_S,Constants.DEFAULTFASTESTINTERVAL));
            if(radius != old_radius){
                al_activity_rv.add(0, "updated Radius to " + radius + "m");
                mAdapter.notifyItemInserted(0);
                old_radius=radius;
                MyProperties.getInstance().Radius=radius;
            }
            if(interval != old_interval){
                al_activity_rv.add(0, "updated Interval Location Updates Frequency to " + interval + "s");
                mAdapter.notifyItemInserted(0);
                old_interval=interval;
                MyProperties.getInstance().Interval=interval;
            }
            if(fastestInterval != old_fastestInterval){
                al_activity_rv.add(0, "updated Fastest Location Updates Frequency to " + fastestInterval + "s");
                mAdapter.notifyItemInserted(0);
                old_fastestInterval = fastestInterval;
                MyProperties.getInstance().Fastest_Interval = fastestInterval;
            }
        }

        @Override
        public void onDrawerStateChanged(int i) {

        }
    };

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if(requestCode == Constants.FINE_LOCATION_PERMESIION_REQUEST_CODE){
            if(grantResults.length>0&&
                    grantResults[0]==PackageManager.PERMISSION_GRANTED){
                initializeAndStart();
            }
            else{
                AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
                builder.setMessage(R.string.dialog_message)
                        .setTitle(R.string.dialog_title)
                        .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                // FIRE ZE MISSILES!
                            }
                        });

                AlertDialog dialog = builder.create();
                dialog.show();
            }
        }
    }
}
