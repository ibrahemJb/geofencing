package com.geofencing.balout.geofencing;

import android.Manifest;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.location.Location;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.speech.tts.TextToSpeech;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.NotificationManagerCompat;
import android.util.Log;
import android.widget.Toast;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.Geofence;
import com.google.android.gms.location.GeofencingClient;
import com.google.android.gms.location.GeofencingRequest;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.tasks.OnCanceledListener;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;

import java.util.ArrayList;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

public class geofencingForegroundService extends Service {
    private FusedLocationProviderClient mFusedLocationClient;
    private int Interval, Fastest_interval, Radius;
    private ArrayList<Geofence> mGeofenceList=null;
    public static Location mCurrentLocation;
    private LocationRequest mLocationRequest;
    private LocationCallback mLocationCallback;
    private GeofencingClient mGeofencingClient;
    private PendingIntent mGeofencePendingIntent = null;
    private static TextToSpeech ttsobj = null;
    private BroadcastReceiver broadcastReceiver;
    private long locationUpdatedAt = Long.MIN_VALUE;

    @Override
    public IBinder onBind(Intent intent) {
        // TODO: Return the communication channel to the service.
        throw new UnsupportedOperationException("Not yet implemented");
    }

    @Override
    public void onCreate() {
        super.onCreate();
        MyProperties.getInstance().isServiceRunning=false;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        if (intent != null) {
            String action = intent.getAction();

            switch (action) {
                case Constants.ACTION_START_FOREGROUND_SERVICE:
                    Bundle b = intent.getBundleExtra("pram");
                    Radius = b.getInt(Constants.RADUIS_S);
                    Interval = b.getInt(Constants.INTERVAL_S);
                    Fastest_interval = b.getInt(Constants.FASTEST_INTERVAL_S);
                    if(MyProperties.getInstance().isServiceRunning==false)
                        startForegroundService();
                    break;
                case Constants.ACTION_STOP_FOREGROUND_SERVICE:
                    stopForegroundService();
                    break;
            }
        }
        return super.onStartCommand(intent, flags, startId);
    }

    private void startForegroundService() {
        MyProperties.getInstance().isServiceRunning=true;
        MyProperties.getInstance().triggerdGeofences = new ArrayList<>();
        initializeTTS();
        initializeFusedLocationClient();
        initializeLocationRequest();
        initializeGeofencingClient();
        registerBroadcastReceivers();
        startForeground(3000, createForegroundServiceNotification());
    }

    //===========database start===================
    private void initializeCoordinatesList() {
        //===========get geofencing list from db ===============
        Database db = new Database(getBaseContext());
        mGeofenceList = db.initializeDatabase();
    }
    //===========database end===================

    //============Notification start===================
    private Notification createForegroundServiceNotification() {
        //============create the notification for the foreground service

        NotificationCompat.Builder builder = new NotificationCompat.Builder(this, Constants.FOREGROUNDCHANNELID);
        builder.setWhen(0);
        builder.setContentTitle("geofencing");
        builder.setContentText("geofencing in the background");
        builder.setSmallIcon(R.mipmap.ic_launcher);
        builder.setPriority(NotificationCompat.PRIORITY_LOW);
        Intent snoozeIntent = new Intent(getApplicationContext(), stopForegroundService.class);
        snoozeIntent.setAction("stopForeground");
        PendingIntent snoozePendingIntent =
                PendingIntent.getBroadcast(this, 0, snoozeIntent, 0);
        builder.addAction(R.drawable.ic_stop_black_24dp,"Stop Geofencing",snoozePendingIntent);
        Notification notification = builder.build();
        return notification;
    }
    //============Notification end===================

    //===============location start====================
    private void initializeFusedLocationClient() {
        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(this);
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        mFusedLocationClient.getLastLocation()
                .addOnSuccessListener(new OnSuccessListener<Location>() {
                    @Override
                    public void onSuccess(Location location) {
                        // Got last known location. In some rare situations this can be null.
                        if (location != null) {
                            mCurrentLocation = location;
                            MyProperties.getInstance().currentLocation = mCurrentLocation;
                            locationUpdatedAt = System.currentTimeMillis();
                            initializeCoordinatesList();
                        }
                    }
                });
    }

    protected void initializeLocationRequest() {
        //create and initialize location request with interval and fastest interval.
        //initialize location call back to handle location changes.
        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(Interval);
        mLocationRequest.setFastestInterval(Fastest_interval);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        mLocationCallback = new LocationCallback() {
            @Override
            public void onLocationResult(LocationResult locationResult) {
                boolean updateLocationandReport = false;
                if (locationResult == null) {
                    return;
                }
                long secondsElapsed = TimeUnit.MILLISECONDS.toSeconds(System.currentTimeMillis() - locationUpdatedAt);
                if (secondsElapsed >= Interval){
                    locationUpdatedAt = System.currentTimeMillis();
                    updateLocationandReport = true;
                }
                if(updateLocationandReport){
                    handleLocationChange(locationResult.getLastLocation());
                }
            }
        };
        startLocationUpdates();
    }

    private void startLocationUpdates() {
        //start the location updates with the location request and the location changes handler
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        mFusedLocationClient.requestLocationUpdates(mLocationRequest,
                mLocationCallback,
                null);
    }

    private void stopLocationUpdates() {
        mFusedLocationClient.removeLocationUpdates(mLocationCallback);
    }
    //===============location end====================

    //==============geofencing start===================
    private void initializeGeofencingClient() {
        mGeofencingClient = LocationServices.getGeofencingClient(this);
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        if(mGeofenceList != null && mGeofenceList.size()>0) {
            mGeofencingClient.addGeofences(getGeofencingRequest(), getGeofencePendingIntent())
                    .addOnSuccessListener(new OnSuccessListener<Void>() {
                        @Override
                        public void onSuccess(Void aVoid) {
                            Log.d("Geofencing add", "onSuccess: addGeofences");
                        }
                    })
                    .addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception e) {
                            Log.d("Geofencing add", "OnFailure: addGeofences");

                        }
                    });
        }
    }

    private GeofencingRequest getGeofencingRequest() {
        GeofencingRequest.Builder builder = new GeofencingRequest.Builder();
        builder.setInitialTrigger(GeofencingRequest.INITIAL_TRIGGER_ENTER);
        builder.addGeofences(mGeofenceList);
        return builder.build();
    }

    private PendingIntent getGeofencePendingIntent() {
        // Reuse the PendingIntent if we already have it.
        if (mGeofencePendingIntent != null) {
            return mGeofencePendingIntent;
        }
        //Intent intent = new Intent("ACTION_RECIVE_GEOFENCE_TRANSITIONS");
        Intent intent = new Intent(this,GeofenceTransitionsIntentService.class);
        mGeofencePendingIntent = PendingIntent.getService(this, 0, intent, PendingIntent.
                FLAG_UPDATE_CURRENT);
        return mGeofencePendingIntent;
    }

    public void stopGeofencing() {
        mGeofencingClient.removeGeofences(getGeofencePendingIntent())
        .addOnSuccessListener(new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void aVoid) {
                Log.d("Geofencing remove", "onSuccess: stopgeofencing");
            }
        })
        .addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                Log.d("Geofencing remove", "OnFailure: stopgeofencing");

            }
        });
    }

    private void updateGeofencingList() {
        mGeofencingClient.removeGeofences(getGeofencePendingIntent())
                .addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        Log.d("Geofencing remove", "onSuccess: stopgeofencing");
                        initializeCoordinatesList();
                        if (ActivityCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                            return;
                        }
                        mGeofencingClient.addGeofences(getGeofencingRequest(), getGeofencePendingIntent())
                                .addOnSuccessListener(new OnSuccessListener<Void>() {
                                    @Override
                                    public void onSuccess(Void aVoid) {
                                        Log.d("Geofencing add", "onSuccess: addGeofences");
                                    }
                                })
                                .addOnFailureListener(new OnFailureListener() {
                                    @Override
                                    public void onFailure(@NonNull Exception e) {
                                        Log.d("Geofencing add", "OnFailure: addGeofences");

                                    }
                                });
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Log.d("Geofencing remove", "OnFailure: stopgeofencing");

                    }
                });

    }
    //=============geofencing end======================

    private void registerBroadcastReceivers() {
        broadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                updateGeofencingList();
            }
        };
        IntentFilter progressfilter = new IntentFilter(Constants.ACTION_UPDATE_GEOFENCELIST);
        registerReceiver(broadcastReceiver, progressfilter);
    }

    private void initializeTTS() {
        ttsobj = new TextToSpeech(getApplicationContext(), new TextToSpeech.OnInitListener() {
            @Override
            public void onInit(int status) {
                if (status != TextToSpeech.ERROR) {
                    ttsobj.setLanguage(Locale.US);
                }
            }
        });
    }
    public static void speaK(String msg) {
        ttsobj.speak("Warning", TextToSpeech.QUEUE_ADD, null);
        ttsobj.speak(msg, TextToSpeech.QUEUE_ADD, null);
    }

    private void stopForegroundService() {

        // Stop foreground service and remove the notification.
        stopForeground(true);

        // Stop the foreground service.
        stopSelf();
    }

    private void handleLocationChange(Location location){
        mCurrentLocation = location;
        MyProperties.getInstance().currentLocation = mCurrentLocation;
        updateGeofencingList();
    }
    @Override
    public void onDestroy() {
        super.onDestroy();
        unregisterReceiver(broadcastReceiver);
        stopGeofencing();
        stopLocationUpdates();
        //stopForegroundService();
    }
}
