package com.geofencing.balout.geofencing;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;

public class stopForegroundService extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        if(intent!=null){
            if(intent.getAction().equals("stopForeground")){
                Intent stopIntent = new Intent(context,geofencingForegroundService.class);
                stopIntent.setAction(Constants.ACTION_STOP_FOREGROUND_SERVICE);
                Intent filter= new Intent("finishActivity");
                context.sendBroadcast(filter);
                context.startService(stopIntent);
            }
        }
    }
}
