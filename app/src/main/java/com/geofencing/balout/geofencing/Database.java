package com.geofencing.balout.geofencing;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.location.Location;
import android.util.Log;

import com.google.android.gms.location.Geofence;

import java.io.File;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import static android.content.Context.MODE_PRIVATE;

public class    Database {
    private Context mContext;
    private ArrayList<String> s = new ArrayList<>();
    private ArrayList<Geofence> mGeofenceList = new ArrayList<>();
    private ArrayList<Place> mPlaceList = new ArrayList<>();

    public Database(Context z){
        mContext = z;
    }

    public ArrayList<Geofence> initializeDatabase() {
        if (!doesDatabaseExist(mContext, Constants.DATABASE_NAME))
            createDatabase();
        // get a reference
        SQLiteDatabase checkDB = SQLiteDatabase
                .openDatabase(mContext.getDatabasePath(Constants.DATABASE_NAME).getAbsolutePath(), null,
                        SQLiteDatabase.OPEN_READONLY);
        addDatabaseEntriesToGeoFenceList(checkDB);
        checkDB.close();
        return mGeofenceList;
    }

    public void createDatabase() {
        //open the file , to copy it into a string buffer(builder),  then split the buffer contents , and finally add them to the
        // database
        // the records file is found in  res/raw
        SQLiteDatabase mydatabase = null;
        InputStream in =mContext.getResources().openRawResource(R.raw.coordinates);
        StringBuilder builder = new StringBuilder();
        // reading the file bytes , copying it into the buffer
        try {
            int count = 0;
            byte[] bytes = new byte[32768];
            while ((count = in.read(bytes, 0, 32768)) > 0) {
                builder.append(new String(bytes, 0, count));
            }
            in.close();
            // builder is a string buffer that contains all the File  lines ( and each line consists of ** log,lat,type **)
            String[] coordinates = builder.toString().split("\n");
            mydatabase = mContext.openOrCreateDatabase(Constants.DATABASE_NAME, MODE_PRIVATE, null);
            mydatabase.execSQL(
                    "CREATE TABLE IF NOT EXISTS GPS(Id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, long REAL,lat REAL,type VARCHAR);");
            for (String line : coordinates) {
                String[] columns = line.split(":");
                String query =
                        "INSERT INTO GPS (long,lat,type) VALUES ("+columns[0] +","+columns[1]+",'"+columns[2].replaceAll("(\\r|\\n)", "")+"')";
                mydatabase.execSQL(query);
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (mydatabase != null) {
                mydatabase.close();
            }
        }
    }
    public static boolean doesDatabaseExist(Context context, String dbName) {
        File dbFile = context.getDatabasePath(dbName);
        return dbFile.exists();
    }

    public void insertIntoDB(String latt, String longt, String type) {
        try {
            SQLiteDatabase checkDB = mContext.openOrCreateDatabase(Constants.DATABASE_NAME, MODE_PRIVATE, null);
            String query =
                    "INSERT INTO GPS (long,lat,type) VALUES ("+longt +","+latt+",'"+type.replaceAll("(\\r|\\n)", "")+"')";
            checkDB.execSQL(query);
            mContext.sendBroadcast(new Intent().setAction(Constants.ACTION_UPDATE_UI).putExtra("msg", String.format("Inserted (%s,%s) with type: %s", latt, longt, type)));
        } catch (Exception e) {
            Log.d("exception", e.toString());
        }
    }
    public void addDatabaseEntriesToGeoFenceList(SQLiteDatabase DB) {
        if(mGeofenceList.size()>0)
            mGeofenceList.clear();
        if(mPlaceList.size()>0)
            mPlaceList.clear();

        Cursor cursor = DB.rawQuery("select DISTINCT * from GPS", null);
        cursor.move(-1);
        int i=0;
        while (cursor.moveToNext()) {
            int Radius = MyProperties.getInstance().Radius;
            mPlaceList.add(getPlace(cursor.getString(3)+"-"+Integer.toString(cursor.getInt(0)),
                                        cursor.getDouble(2),cursor.getDouble(1),Radius,cursor.getString(3)));
        }
        if(mPlaceList.size()>100){
            Place p = new Place(null,MyProperties.getInstance().currentLocation,"");
            Collections.sort(mPlaceList,new SortPlaces(p));
        }
        for(int j=0;j<mPlaceList.size() && j <100;j++){
            mGeofenceList.add(mPlaceList.get(j).geofence);
            Log.d("added a geofence",mPlaceList.get(j).location.getLatitude() + " - " + mPlaceList.get(j).location.getLongitude()+"-" + mPlaceList.get(j).geofence.getRequestId() );

        }
        Log.d("gefence list updated","places list size = "+ Integer.toString(mPlaceList.size()) + "\ngeofence list size = "+Integer.toString(mGeofenceList.size()));
    }

    public Place getPlace(String requestId,double latitude,double longitude,int radius,String type){
        Location location = new Location("");
        location.setLatitude(latitude);
        location.setLongitude(longitude);
        return new Place(new Geofence.Builder()
                .setRequestId(requestId)
                .setCircularRegion(
                        latitude,
                        longitude,
                        radius
                )
                .setExpirationDuration(Geofence.NEVER_EXPIRE)
                .setTransitionTypes(Geofence.GEOFENCE_TRANSITION_ENTER | Geofence.GEOFENCE_TRANSITION_EXIT)
                .build(),location,type);
    }
}


class SortPlaces implements Comparator<Place> {
    Place currentLoc;
    public SortPlaces(Place current){
        currentLoc = current;
    }
    @Override
    public int compare(final Place place1, final Place place2) {
        double lat1 = place1.location.getLatitude();
        double lon1 = place1.location.getLongitude();
        double lat2 = place2.location.getLatitude();
        double lon2 = place2.location.getLongitude();

        double distanceToPlace1 = distance(currentLoc.location.getLatitude(), currentLoc.location.getLongitude(), lat1, lon1);
        double distanceToPlace2 = distance(currentLoc.location.getLatitude(), currentLoc.location.getLongitude(), lat2, lon2);
        return (int) (distanceToPlace1 - distanceToPlace2);
    }

    public double distance(double fromLat, double fromLon, double toLat, double toLon) {
        double radius = 6378137;   // approximate Earth radius, *in meters*
        double deltaLat = toLat - fromLat;
        double deltaLon = toLon - fromLon;
        double angle = 2 * Math.asin( Math.sqrt(
                Math.pow(Math.sin(deltaLat/2), 2) +
                        Math.cos(fromLat) * Math.cos(toLat) *
                                Math.pow(Math.sin(deltaLon/2), 2) ) );
        return radius * angle;
    }
}
