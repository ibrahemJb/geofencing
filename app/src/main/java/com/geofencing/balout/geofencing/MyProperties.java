package com.geofencing.balout.geofencing;

import android.location.Location;

import com.google.android.gms.location.Geofence;

import java.util.List;

public class MyProperties {
    private static MyProperties mInstance= null;

    public int Radius;
    public int Interval;
    public int Fastest_Interval;
    public boolean StopSigns;
    public boolean TrafficLights;
    public boolean Humps;
    public boolean PedestrainCrossings;
    public boolean YeildSigns;
    public boolean HumpSigns;
    public boolean PedestrainCrossingsSigns;
    public Location currentLocation;
    public List<Geofence> triggerdGeofences;
    public boolean isServiceRunning;

    protected MyProperties(){}

    public static synchronized MyProperties getInstance() {
        if(null == mInstance){
            mInstance = new MyProperties();
        }
        return mInstance;
    }
}
