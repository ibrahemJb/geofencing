package com.geofencing.balout.geofencing;


import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.TextView;
import java.util.ArrayList;

public class notificationAdapter extends
        RecyclerView.Adapter<notificationAdapter.ViewHolder> {

    private ArrayList<String> info = null;

    public notificationAdapter(ArrayList<String> info){
        this.info = info;
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements OnClickListener {
        // Your holder should contain a member variable
        // for any view that will be set as you render a row
        public TextView nameTextView;

        // We also create a constructor that accepts the entire item row
        // and does the view lookups to find each subview
        public ViewHolder(View itemView) {
            // Stores the itemView in a public final member variable that can be used
            // to access the context from any ViewHolder instance.
            super(itemView);
//      nameTextView.setOnClickListener(this);
            nameTextView = (TextView) itemView.findViewById(R.id.warning_info);
            nameTextView.setText("");
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            if(view.equals(itemView)){
                removeAt(getAdapterPosition());
            }
        }
        public void removeAt(int position) {
            try {
                info.remove(position);
                notifyItemRemoved(position);
                notifyItemRangeChanged(position, info.size());
            }catch(Exception e){}
        }
    }

    // Usually involves inflating a layout from XML and returning the holder
    @Override
    public notificationAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        Context context = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(context);

        // Inflate the custom layout
        View contactView = inflater.inflate(R.layout.activity_item, parent, false);

        // Return a new holder instance
        ViewHolder viewHolder = new ViewHolder(contactView);
        return viewHolder;
    }

    // Involves populating data into the item through holder
    @Override
    public void onBindViewHolder(notificationAdapter.ViewHolder viewHolder, int position) {
        String inf = info.get(position);
        TextView display = viewHolder.nameTextView;
        display.setText(inf);
    }

    // Returns the total count of items in the list
    @Override
    public int getItemCount() {

        return info.size();
    }

}
