package com.geofencing.balout.geofencing;

public class Constants {
    public static final String TYPE_HUMP = "Hump.";
    public static final String TYPE_HUMP_SIGN = "Hump Sign.";
    public static final String TYPE_PEDESTRAIN_CROSSING = "Pedestrian Crossing.";
    public static final String TYPE_PEDESTRAIN_CROSSING_SIGN = "Pedestrian Crossing Sign.";
    public static final String TYPE_TRAFFIC_LIGHT = "Traffic Light.";
    public static final String TYPE_STOP_SIGN = "Stop Sign.";
    public static final String TYPE_YIELD_SIGN = "Yield Sign.";
    public static final String DATABASE_NAME = "gps.db";

    public static final String DEFAULTRADIUS = "100";
    public static final String DEFAULTINTERVAL = "10";
    public static final String DEFAULTFASTESTINTERVAL = "5";

    public static final String CHANNELID = "geofencing_channel_id";
    public static final String CHANNELNAME = "geofencing";
    public static final String CHANNELDESCRIPTION = "geofencing channel desc";

    public static final String FOREGROUNDCHANNELID = "foreground_channel_id";
    public static final String FOREGROUNDCHANNELNAME = "foreground service channgel";
    public static final String FOREGROUNDCHANNELDESCRIPTION = "foreground service channel desc";

    public static final String RADUIS_S="Radius";
    public static final String INTERVAL_S="Interval";
    public static final String FASTEST_INTERVAL_S="Fastest_Interval";
    public static final String INITIALIZED_S="Initialized";
    public static final String STOPSIGN_S="Stop_Sign";
    public static final String TRAFFIC_LIGHT_S="Traffic_Light";
    public static final String HUMP_S="Humps";
    public static final String PEDESTRAIN_CROSSING_S="Pedestrain_Crossing";
    public static final String YEILD_SIGN_S="Yeild_Sign";
    public static final String HUMP_SIGN_S="Hump_Sign";
    public static final String PEDESTRAIN_CROSSING_SIGN_S="Pedestrain_Crossing_Sign";

    public static final String ACTION_START_FOREGROUND_SERVICE="ACTION_START_FOREGROUND_SERVICE";
    public static final String ACTION_STOP_FOREGROUND_SERVICE="ACTION_STOP_FOREGROUND_SERVICE";
    public static final String ACTION_UPDATE_UI = "ACTION_UPDATE_UI";
    public static final String ACTION_UPDATE_GEOFENCELIST = "ACTION_UPDATE_GEOFENCE_LIST";
    public static final String ACTION_RECIVE_GEOFENCE_TRANSITIONS = "ACTION_RECIVE_GEOFENCE_TRANSITIONS";

    public static final int FINE_LOCATION_PERMESIION_REQUEST_CODE=100;
}
