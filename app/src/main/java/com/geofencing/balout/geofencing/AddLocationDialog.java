package com.geofencing.balout.geofencing;

import android.app.Dialog;
import android.content.Context;
import android.location.Location;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;

import com.google.android.gms.location.Geofence;

public class AddLocationDialog extends Dialog
implements View.OnClickListener {

    public void setMyDialogListener(MyDialogListener myDialogListener) {
        this.myDialogListener = myDialogListener;
    }

    private MyDialogListener myDialogListener;
    private Button getLocation;
    private Button submit;
    private EditText lattitude,longitude;
    private Spinner spinnerType;
    public AddLocationDialog(@NonNull Context context) {
        super(context);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.add_location_dialog_layout);
        getLocation= findViewById(R.id.grab_location);
        submit= findViewById(R.id.submit_location);
        lattitude=findViewById(R.id.add_latitude);
        longitude=findViewById(R.id.add_longitude);
        spinnerType=findViewById(R.id.add_type);
        getLocation.setOnClickListener(this);
        submit.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.grab_location:{
                Location currentLocation =  MyProperties.getInstance().currentLocation;
                lattitude.setText(Double.toString(currentLocation.getLatitude()));
                longitude.setText(Double.toString(currentLocation.getLongitude()));
                break;
            }
            case R.id.submit_location:{
                if(!lattitude.getText().toString().isEmpty()
                        &&!longitude.getText().toString().isEmpty()){
                    myDialogListener.userSubmitedAValue(lattitude.getText().toString(),
                            longitude.getText().toString(),(String)spinnerType.getSelectedItem());
                    break;
                }
            }
        }
    }
}
